# Build XPERIA XZ PREMIUM inside Docker using gitlab-ci

All started for this link https://developer.sony.com/develop/open-devices/guides/aosp-build-instructions/build-aosp-android-android-10-0-0

Change or edit this repo depeding to your needs. You need change some variable

This are the two parameters to build android 10 for any xperia device in the list below

> $Branch = android-10.0.0_r41

> $target_device = aosp_g8141-userdebug // You can change thisby the one in the list below

Run: `source build/envsetup.sh && lunch $target_device`

Example for this build source :

Run `source build/envsetup.sh && lunch aosp_g8141-userdebug`

Then run : 
`make -j$(nproc)`

# This is the list of target devices

1. aosp_arm-eng
2. aosp_arm64-eng
3. aosp_coral_car-userdebug
4. aosp_crosshatch_car-userdebug
5. aosp_g8141-eng
6. aosp_g8141-userdebug
7. aosp_g8142-eng
8. aosp_g8142-userdebug
9. aosp_g8341-eng
10. aosp_g8341-userdebug
11. aosp_g8342-eng
12. aosp_g8342-userdebug
13. aosp_g8441-eng
14. aosp_g8441-userdebug
15. aosp_h3113-eng
16. aosp_h3113-userdebug
17. aosp_h3213-eng
18. aosp_h3213-userdebug
19. aosp_h3413-eng
20. aosp_h3413-userdebug
21. aosp_h4113-eng
22. aosp_h4113-userdebug
23. aosp_h4213-eng
24. aosp_h4213-userdebug
25. aosp_h4413-eng
26. aosp_h4413-userdebug
27. aosp_h8216-eng
28. aosp_h8216-userdebug
29. aosp_h8266-eng
30. aosp_h8266-userdebug
31. aosp_h8314-eng
32. aosp_h8314-userdebug
33. aosp_h8324-eng
34. aosp_h8324-userdebug
35. aosp_h8416-eng
36. aosp_h8416-userdebug
37. aosp_h9436-eng
38. aosp_h9436-userdebug
39. aosp_i3113-eng
40. aosp_i3113-userdebug
41. aosp_i3213-eng
42. aosp_i3213-userdebug
43. aosp_i4113-eng
44. aosp_i4113-userdebug
45. aosp_i4213-eng
46. aosp_i4213-userdebug
47. aosp_j8110-eng
48. aosp_j8110-userdebug
49. aosp_j8210-eng
50. aosp_j8210-userdebug
51. aosp_j9110-eng
52. aosp_j9110-userdebug
53. aosp_j9210-eng
54. aosp_j9210-userdebug
55. aosp_x86-eng
56. aosp_x86_64-eng
57. aosp_xqau51-eng
58. aosp_xqau51-userdebug
59. aosp_xqau52-eng
60. aosp_xqau52-userdebug
