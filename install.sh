#!/bin/bash

dpkg --add-architecture i386
apt-get update -y && apt-get upgrade -y
apt-get install openjdk-11-jdk -y
java -version

apt-get install python3 curl bison g++-multilib git gperf libxml2-utils make zlib1g-dev:i386 zip liblz4-tool libncurses5 libssl-dev bc flex -y

ln -s /usr/bin/python3 /usr/bin/python


git config --global user.email "fatih.gucuko@webinify.com"
git config --global user.name "Fatih Gucuko"

mkdir ~/bin
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
export PATH=~/bin:$PATH

mkdir ~/android
cd ~/android
repo init -u https://android.googlesource.com/platform/manifest -b android-10.0.0_r41

cd .repo
git clone https://github.com/sonyxperiadev/local_manifests
cd local_manifests
git checkout android-10.0.0_r41
cd ../..

repo sync
./repo_update.sh
echo "updating repo finished"
# source build/envsetup.sh && lunch

